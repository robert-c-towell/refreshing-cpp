# refreshing-cpp

Following the Udemy course The C++20 Masterclass: From Fundamentals to Advanced by Daniel Gakwaya. Refreshing my skills after not using C++ for 7 years.

Below are notes to myself.

## Compilers

According to Apple, Clang is much faster, uses less memory, and provides more useful error messages than GCC.

#### GCC

```
sudo apt-cache search gcc | less
sudo apt-get install gcc-11 g++-11 -y
gcc --version
```

#### Clang

```
sudo apt-cache search clang | less
sudo apt-get install clang-14 -y
clang++-14 --version
```

## Debugging

```
sudo apt-get install gdb
```
