# Notes

## Syntax

### Aggregate (POD)

Variable initialization via the POD (Plain Old Data) class looks like:

```
int x = {7};

struct point3D {
  int x;
  int y;
  int z;
}

point3D p = {1,2,3}
```

It only works if there is no user-defined constructor.
