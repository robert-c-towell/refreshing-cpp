#include <iostream>
#include <iomanip>

int main()
{
  float f{1.12345678901234567890f};
  double d{1.123456789012345567890};
  long double ld{1.12345678901234567890L};

  std::cout << "sizeof float: " << sizeof(float) << std::endl;
  std::cout << "sizeof double: " << sizeof(double) << std::endl;
  std::cout << "sizeof long double: " << sizeof(long double) << std::endl;

  std::cout << std::setprecision(20);
  std::cout << "f is: " << f << std::endl;
  std::cout << "d is: " << d << std::endl;
  std::cout << "ld is: " << ld << std::endl;

  float tooBig = 192400023.0f;
  std::cout << "tooBig: " << tooBig << std::endl;

  return 0;
}