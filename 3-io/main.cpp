#include <iostream>

int add(int a, int b)
{
  int sum = a + b;
  std::cout << "Sum: " << sum << std::endl;
  return sum;
}

int diff(int a, int b)
{
  int diff = a - b;
  if (diff < 0)
  {
    diff *= -1;
  }
  std::cout << "Diff: " << diff << std::endl;
  return diff;
}

int main()
{
  int n1{0};
  int n2{0};

  std::cout << "Enter two numbers: " << std::endl;
  std::cin >> n1 >> n2;

  std::cout << std::endl;

  add(n1, n2);
  diff(n1, n2);

  return 0;
}